package app.crescentpay.src.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionConfidence;
import org.bitcoinj.script.Script;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.KeyChain;
import org.bitcoinj.wallet.Wallet;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.crescentpay.src.MainActivity;
import app.crescentpay.src.R;
import app.crescentpay.src.qr.QRHelper;
import app.crescentpay.src.utils.Constants;

public class UIHelper {

    private Button createWalletBtn;
    private Button restoreWalletBtn;
    public FrameLayout restore_wallet;
    public FrameLayout new_wallet;
    public FrameLayout newuser;
    private Button qrScan;
    private ImageButton openKeys;
    private TextView balance;

    /////REGISTER SCREEN
    private Button registerUserBtn;
    public EditText handle;
    /////REGISTER SCREEN

    /////RECOVERY SCREEN
    private Button verifyUserBtn;
    public EditText recoverySeed;
    public EditText handle2;
    /////RECOVERY SCREEN


    /////WALLET SETTINGS SCREEN
    public FrameLayout walletSettings;
    private TextView seed;
    private TextView xpub;
    private Button closeSettingsBtn;
    /////WALLET SETTINGS SCREEN

    /////RECEIVE BITCOIN STUFF
    private ImageView copyBtcAddr;
    private ImageView btcQR;
    private TextView btcAddress;
    /////RECEIVE BITCOIN STUFF

    private TextView titleLabel;
    private FrameLayout flDownloadContent_LDP;
    private ProgressBar pbProgress_LDP;
    private TextView tvPercentage_LDP;
    private SwipeRefreshLayout srlContent_AM;
    private SwipeRefreshLayout srlHistory;
    private TextView myCashHandle;
    private TextView tvRecipientAddress_AM;
    private TextView etAmount_AM;
    private Button btnSend_AM;
    private Button closeSendBtn;
    private ImageView ivCopy_AM;
    private ImageButton sendTab;
    private ImageButton receiveTab;
    private Button closeReceiveBtn;
    public FrameLayout sendWindow;
    public FrameLayout receiveWindow;
    public FrameLayout historyWindow;
    private ImageButton historyTab;
    private Button closeHistoryBtn;
    private Button setMaxCoins;
    private NonScrollListView txHistoryList;
    private FrameLayout txInfo;
    private TextView txInfoTV;
    private Button btnViewTx;
    private Button btnCloseTx;
    public TextView feeText;

    public UIHelper()
    {
        MainActivity mainActivity = MainActivity.INSTANCE;

        createWalletBtn = mainActivity.findViewById(R.id.createWalletBtn);
        restoreWalletBtn = mainActivity.findViewById(R.id.restoreWalletBtn);
        restore_wallet = mainActivity.findViewById(R.id.restore_wallet);
        new_wallet = mainActivity.findViewById(R.id.new_wallet);
        newuser = mainActivity.findViewById(R.id.newuser);
        qrScan = mainActivity.findViewById(R.id.qrScan);
        openKeys = mainActivity.findViewById(R.id.openKeys);
        balance = mainActivity.findViewById(R.id.balance);
        registerUserBtn = mainActivity.findViewById(R.id.registerUserBtn);
        handle = mainActivity.findViewById(R.id.handle);
        verifyUserBtn = mainActivity.findViewById(R.id.verifyUserBtn);
        recoverySeed = mainActivity.findViewById(R.id.recoverySeed);
        handle2 = mainActivity.findViewById(R.id.handle2);
        walletSettings = mainActivity.findViewById(R.id.walletSettings);
        seed = mainActivity.findViewById(R.id.seed);
        xpub = mainActivity.findViewById(R.id.xpub);
        closeSettingsBtn = mainActivity.findViewById(R.id.closeSettingsBtn);
        copyBtcAddr = mainActivity.findViewById(R.id.copyBtcAddr);
        btcQR = mainActivity.findViewById(R.id.btcQR);
        btcAddress = mainActivity.findViewById(R.id.btcAddress);
        titleLabel = mainActivity.findViewById(R.id.titleLabel);
        flDownloadContent_LDP = mainActivity.findViewById(R.id.flDownloadContent_LDP);
        pbProgress_LDP = mainActivity.findViewById(R.id.pbProgress_LDP);
        tvPercentage_LDP = mainActivity.findViewById(R.id.tvPercentage_LDP);
        srlContent_AM = mainActivity.findViewById(R.id.srlContent_AM);
        srlHistory = mainActivity.findViewById(R.id.srlHistory);
        myCashHandle = mainActivity.findViewById(R.id.myCashHandle);
        tvRecipientAddress_AM = mainActivity.findViewById(R.id.tvRecipientAddress_AM);
        etAmount_AM = mainActivity.findViewById(R.id.etAmount_AM);
        btnSend_AM = mainActivity.findViewById(R.id.btnSend_AM);
        closeSendBtn = mainActivity.findViewById(R.id.closeSendBtn);
        ivCopy_AM = mainActivity.findViewById(R.id.ivCopy_AM);
        sendTab = mainActivity.findViewById(R.id.sendTab);
        receiveTab = mainActivity.findViewById(R.id.receiveTab);
        closeReceiveBtn = mainActivity.findViewById(R.id.closeReceiveBtn);
        sendWindow = mainActivity.findViewById(R.id.sendWindow);
        receiveWindow = mainActivity.findViewById(R.id.receiveWindow);
        historyWindow = mainActivity.findViewById(R.id.historyWindow);
        historyTab = mainActivity.findViewById(R.id.historyTab);
        closeHistoryBtn = mainActivity.findViewById(R.id.closeHistoryBtn);
        setMaxCoins = mainActivity.findViewById(R.id.setMaxCoins);
        txHistoryList = mainActivity.findViewById(R.id.txHistoryList);
        feeText = mainActivity.findViewById(R.id.feeText);
        txInfo = mainActivity.findViewById(R.id.txInfo);
        txInfoTV = mainActivity.findViewById(R.id.txInfoTV);
        btnCloseTx = mainActivity.findViewById(R.id.btnCloseTx);
        btnViewTx = mainActivity.findViewById(R.id.btnViewTx);

        this.initListeners();
    }

    private void initListeners()
    {
        this.restoreWalletBtn.setOnClickListener(v -> displayRestore());
        this.createWalletBtn.setOnClickListener(v -> displayNewWallet());
        this.srlContent_AM.setOnRefreshListener(this::refresh);
        this.srlHistory.setOnRefreshListener(this::refresh);

        this.btnSend_AM.setOnClickListener(v -> MainActivity.INSTANCE.wallet.send());
        this.ivCopy_AM.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("My $handle", myCashHandle.getText().toString());
            ClipboardManager clipboard = (ClipboardManager)MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show();
        });

        this.copyBtcAddr.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("My wallet", btcAddress.getText().toString());
            ClipboardManager clipboard = (ClipboardManager)MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show();
        });

        this.sendTab.setOnClickListener(v -> displaySend());
        this.receiveTab.setOnClickListener(v -> displayReceive());
        this.historyTab.setOnClickListener(v -> displayHistory());
        this.qrScan.setOnClickListener(v -> clickScanQR());
        this.openKeys.setOnClickListener(v -> displayWalletKeys());

        txHistoryList.setOnItemClickListener((parent, view, position, id) -> {
            Transaction tx = MainActivity.INSTANCE.txList.get(position);
            displayTxWindow(tx);
        });


    }

    public void displayTxWindow(Transaction tx) {
        if(!isDisplayingDownload())
        {
            DecimalFormat decimalFormatter = new DecimalFormat("0.########");
            String receivedValueStr = MonetaryFormat.BTC.format(tx.getValue(MainActivity.INSTANCE.wallet.getWallet())).toString();
            receivedValueStr = receivedValueStr.replace("BTC ", "");
            float amtTransferred = Float.parseFloat(receivedValueStr);
            String amtTransferredStr = decimalFormatter.format(Math.abs(amtTransferred));
            String feeStr;

            if (tx.getFee() != null) {
                String feeValueStr = MonetaryFormat.BTC.format(tx.getFee()).toString();
                feeValueStr = feeValueStr.replace("BTC ", "");
                float fee = Float.parseFloat(feeValueStr);
                feeStr = decimalFormatter.format(fee);
            } else {
                feeStr = "n/a";
            }

            TransactionConfidence txConfirmations = tx.getConfidence();

            String txConfirms = "" + txConfirmations.getDepthInBlocks();
            String txDate = tx.getUpdateTime() + "";
            String txHash = tx.getHashAsString();
            txInfo.setVisibility(View.VISIBLE);
            txInfoTV.setText(Html.fromHtml("<b>BTC Transferred:</b> " + amtTransferredStr + "<br> <b>Fee:</b> " + feeStr + "<br> <b>Date:</b> " + txDate + "<br> <b>Confirmations:</b> " + txConfirms));
            btnCloseTx.setOnClickListener(v -> txInfo.setVisibility(View.GONE));
            btnViewTx.setOnClickListener(v -> {
                String url = Constants.IS_PRODUCTION ? "https://oxt.me/transaction/" : "https://testnet.blockchain.info/tx/";
                Uri uri = Uri.parse(url + txHash); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                MainActivity.INSTANCE.startActivity(intent);
            });

        }
    }


    private void displayNewWallet() {
        restore_wallet.setVisibility(View.GONE);
        new_wallet.setVisibility(View.VISIBLE);
        newuser.setVisibility(View.GONE);

        registerUserBtn.setOnClickListener(view -> MainActivity.INSTANCE.netHelper.registerUser());
    }

    private void displayHistory() {
        setArrayAdapter(MainActivity.INSTANCE.wallet.getWallet());

        historyWindow.setVisibility(View.VISIBLE);

        closeHistoryBtn.setOnClickListener(v -> hideHistoryView());
    }

    private void hideHistoryView() {
        historyWindow.setVisibility(View.GONE);
    }

    private void displayWalletKeys() {
        DeterministicSeed seed = MainActivity.INSTANCE.wallet.getWallet().getKeyChainSeed();
        final List<String> mnemonicCode = seed.getMnemonicCode();
        StringBuilder recoverySeed = new StringBuilder();

        assert mnemonicCode != null;
        for (int x = 0; x < mnemonicCode.size(); x++) {
            recoverySeed.append(mnemonicCode.get(x)).append(" ");
        }

        if(!isDisplayingDownload()) {
            setWalletInfo(recoverySeed.toString());
        }
        walletSettings.setVisibility(View.VISIBLE);
        closeSettingsBtn.setOnClickListener(view -> hideWalletKeys());
    }

    private void hideWalletKeys() {
        walletSettings.setVisibility(View.GONE);
    }

    private void displayReceive() {
        displayMyAddress(MainActivity.INSTANCE.wallet.getWallet());
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.VISIBLE);

        closeReceiveBtn.setOnClickListener(view -> hideReceiveView());
    }

    private void hideReceiveView() {
        receiveWindow.setVisibility(View.GONE);
    }

    private void displaySend() {
        sendWindow.setVisibility(View.VISIBLE);
        receiveWindow.setVisibility(View.GONE);

        closeSendBtn.setOnClickListener(view -> hideSendView());
        setMaxCoins.setOnClickListener(view -> setMaxCoins());
    }

    private void setMaxCoins() {
        String coins = MainActivity.INSTANCE.wallet.getWallet().getBalance().toPlainString();

        etAmount_AM.setText(coins);
    }

    private void hideSendView() {
        sendWindow.setVisibility(View.GONE);
        etAmount_AM.setText(null);
        tvRecipientAddress_AM.setText(null);
    }

    private void displayRestore() {
        MainActivity.isNewUser = false;
        restore_wallet.setVisibility(View.VISIBLE);
        new_wallet.setVisibility(View.GONE);
        newuser.setVisibility(View.GONE);

        verifyUserBtn.setOnClickListener(view -> MainActivity.INSTANCE.netHelper.verifyUser());
    }

    @UiThread
    public void displayDownloadContent(boolean isShown) {
        flDownloadContent_LDP.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    private void setArrayAdapter(Wallet wallet)
    {
        setListViewShit(wallet);

        if (srlHistory.isRefreshing()) srlHistory.setRefreshing(false);
    }

    private void setListViewShit(Wallet wallet)
    {
        if(wallet != null) {
            List<Transaction> txListFromWallet = wallet.getRecentTransactions(100, false);

            if (txListFromWallet != null && txListFromWallet.size() != 0) {
                ArrayList<Map<String, String>> txListFormatted = new ArrayList<Map<String, String>>();
                MainActivity.INSTANCE.txList = new ArrayList<Transaction>();

                int sizeToUse;
                if (txListFromWallet.size() >= 100)
                    sizeToUse = 100;
                else
                    sizeToUse = txListFromWallet.size();

                for (int x = 0; x < sizeToUse; x++) {
                    Transaction tx = txListFromWallet.get(x);
                    int confirmations = tx.getConfidence().getDepthInBlocks();
                    Coin value = tx.getValue(wallet);
                    Map<String, String> datum = new HashMap<String, String>(2);

                    if (value.isPositive()) {
                        String receivedValueStr = value.toPlainString();
                        String entry = String.format("▶ %5s BTC", receivedValueStr);
                        datum.put("amount", entry);
                        MainActivity.INSTANCE.txList.add(tx);
                    }

                    if (value.isNegative()) {
                        String sentValueStr = value.toPlainString();
                        String entry = String.format("◀ %5s BTC", sentValueStr.replace("-", ""));
                        datum.put("amount", entry);
                        MainActivity.INSTANCE.txList.add(tx);
                    }

                    if(confirmations == 0) {
                        datum.put("confirmations", "0/unconfirmed");
                    } else if(confirmations < 6){
                        datum.put("confirmations", confirmations + "/6 confirmations");
                    } else {
                        datum.put("confirmations", "6+ confirmations");
                    }

                    txListFormatted.add(datum);
                }

                SimpleAdapter itemsAdapter = new SimpleAdapter(MainActivity.INSTANCE, txListFormatted, android.R.layout.simple_list_item_2, new String[] {"amount", "confirmations"}, new int[] {android.R.id.text1, android.R.id.text2}){
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent){
                        // Get the Item from ListView
                        View view = super.getView(position, convertView, parent);

                        // Initialize a TextView for ListView each Item
                        TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                        TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                        // Set the text color of TextView (ListView Item)
                        text1.setTextColor(Color.BLACK);
                        text2.setEllipsize(TextUtils.TruncateAt.END);
                        text2.setMaxLines(1);
                        text2.setSingleLine(true);

                        // Generate ListView Item using TextView
                        return view;
                    }
                };

                MainActivity.INSTANCE.runOnUiThread(() -> txHistoryList.setAdapter(itemsAdapter));

                int desiredWidth = View.MeasureSpec.makeMeasureSpec(txHistoryList.getWidth(), View.MeasureSpec.UNSPECIFIED);
                int totalHeight = 0;
                View view = null;

                for (int i = 0; i < itemsAdapter.getCount(); i++) {
                    view = itemsAdapter.getView(i, view, txHistoryList);

                    if (i == 0)
                        view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

                    view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += view.getMeasuredHeight();
                }

                ViewGroup.LayoutParams params = txHistoryList.getLayoutParams();
                params.height = totalHeight + (txHistoryList.getDividerHeight() * (itemsAdapter.getCount() - 1));

                MainActivity.INSTANCE.runOnUiThread(() -> {
                    txHistoryList.setLayoutParams(params);
                    txHistoryList.requestLayout();
                });
            }
        }
    }

    private void clickScanQR() {
        QRHelper qrHelper = new QRHelper();
        qrHelper.startQRScan(MainActivity.INSTANCE);
    }

    public void displayRecipientAddress(String recipientAddress) {
        if(recipientAddress != null) {
            if (TextUtils.isEmpty(recipientAddress)) {
                tvRecipientAddress_AM.setHint(MainActivity.INSTANCE.getResources().getString(R.string.receiver));
            } else {
                tvRecipientAddress_AM.setText(recipientAddress);
            }
        }
        else
        {
            tvRecipientAddress_AM.setText(null);
            tvRecipientAddress_AM.setHint(MainActivity.INSTANCE.getResources().getString(R.string.receiver));
        }
    }

    public void showToastMessage(String message) {
        Toast.makeText(MainActivity.INSTANCE, message, Toast.LENGTH_SHORT).show();
    }

    public String getRecipient() {
        return tvRecipientAddress_AM.getText().toString().trim();
    }

    public boolean isDisplayingDownload() {
        return flDownloadContent_LDP.getVisibility() == View.VISIBLE;
    }

    private void setWalletInfo(String recoverySeed) {
        this.seed.setText(recoverySeed);
        this.xpub.setText(MainActivity.INSTANCE.wallet.getXPUB());
    }

    @UiThread
    public void displayProgress(int percent) {
        if(pbProgress_LDP.isIndeterminate()) pbProgress_LDP.setIndeterminate(false);
        pbProgress_LDP.setProgress(percent);
    }

    @UiThread
    public void displayPercentage(int percent) {
        tvPercentage_LDP.setText(percent + " %");
    }

    @UiThread
    public void displayMyBalance(String myBalance) {
        String balanceStr = myBalance;
        balanceStr = balanceStr.replace(" BTC", "");
        balanceStr = "\u20BF" + balanceStr;
        balance.setText(balanceStr);
    }

    @UiThread
    private void displayMyAddress(Wallet wallet) {
        String handle = "$" + MainActivity.INSTANCE.handleStr;

        myCashHandle.setText(handle);
        btcAddress.setText(wallet.freshReceiveAddress().toString());
        generateQR(btcAddress.getText().toString(), R.id.btcQR);

        if (srlContent_AM.isRefreshing()) srlContent_AM.setRefreshing(false);
    }

    private void generateQR(String textToConvert, int viewID)
    {
        QRCodeWriter btcWriter = new QRCodeWriter();
        try {
            BitMatrix btc_bitMatrix = btcWriter.encode(textToConvert, BarcodeFormat.QR_CODE, 300, 300);
            int width = btc_bitMatrix.getWidth();
            int height = btc_bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, btc_bitMatrix.get(x, y) ? Color.BLACK : Color.parseColor("#00ffffff"));
                }
            }
            ((ImageView) MainActivity.INSTANCE.findViewById(viewID)).setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public TextView getAmountText()
    {
        return etAmount_AM;
    }

    public String getAmount() {
        return etAmount_AM.getText().toString();
    }

    public void clearAmount() { etAmount_AM.setText(null); }

    public void refresh() {
        MainActivity.INSTANCE.wallet.serializedXpub = MainActivity.INSTANCE.wallet.getWallet().getWatchingKey().serializePubB58(MainActivity.INSTANCE.wallet.parameters);
        displayMyBalance(MainActivity.INSTANCE.wallet.getBalance(MainActivity.INSTANCE.wallet.getWallet()).toFriendlyString());
        displayMyAddress(MainActivity.INSTANCE.wallet.getWallet());

        DeterministicSeed seed = MainActivity.INSTANCE.wallet.getWallet().getKeyChainSeed();
        final List<String> mnemonicCode = seed.getMnemonicCode();
        String recoverySeed = "";

        assert mnemonicCode != null;
        for (int x = 0; x < mnemonicCode.size(); x++) {
            recoverySeed += mnemonicCode.get(x) + " ";
        }

        if(!isDisplayingDownload() && historyWindow.getVisibility() == View.VISIBLE) {
            setArrayAdapter(MainActivity.INSTANCE.wallet.getWallet());
            setWalletInfo(recoverySeed);
        }
    }
}
