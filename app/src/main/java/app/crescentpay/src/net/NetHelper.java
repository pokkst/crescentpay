package app.crescentpay.src.net;

import android.text.TextUtils;
import android.view.View;

import com.google.common.base.Splitter;

import org.bitcoinj.crypto.MnemonicCode;
import org.bitcoinj.crypto.MnemonicException;
import org.bitcoinj.wallet.DeterministicSeed;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import app.crescentpay.src.MainActivity;
import app.crescentpay.src.hash.HashHelper;
import app.crescentpay.src.ui.UIHelper;
import app.crescentpay.src.utils.Constants;

import static com.google.common.base.Preconditions.checkArgument;

public class NetHelper {

    private String responseMessage = "null";
    private int responseCode = -1;
    private UIHelper uiHelper;

    private String loginURL = "https://crescentpay.app/" + (Constants.IS_PRODUCTION ? "" : "test/") + "login/";
    private String registerURL = "https://crescentpay.app/"  + (Constants.IS_PRODUCTION ? "" : "test/") + "register/";
    private String getAddrURL = "https://crescentpay.app/"  + (Constants.IS_PRODUCTION ? "" : "test/") + "getaddr/";
    private String checkForUserURL = "https://crescentpay.app/"  + (Constants.IS_PRODUCTION ? "" : "test/") + "checkForUser/";

    public NetHelper()
    {
        uiHelper = MainActivity.INSTANCE.uiHelper;
    }

    public void verifyUser() {
        responseMessage = "null";
        String username = uiHelper.handle2.getText().toString();
        String seedHashed = HashHelper.SHA256(HashHelper.SHA256(uiHelper.recoverySeed.getText().toString() + username));

        login(username, seedHashed);

        while(true)
        {
            System.out.println("Verifying...");

            if(responseMessage.equals("verify.success"))
            {
                MainActivity.INSTANCE.prefs.edit().putBoolean("isNewUser", false).apply();

                MainActivity.INSTANCE.handleStr = username;
                MainActivity.INSTANCE.prefs.edit().putString("handle", username).apply();

                long creationTime = 1551084297L;
                DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(uiHelper.recoverySeed.getText()), null, "", creationTime);

                int length = Splitter.on(' ').splitToList(uiHelper.recoverySeed.getText()).size();

                if (length == 12) {
                    uiHelper.recoverySeed.setText("");
                    MainActivity.INSTANCE.wallet.setupWalletKit(seed, username, seedHashed);
                    uiHelper.displayDownloadContent(true);
                    uiHelper.restore_wallet.setVisibility(View.GONE);
                    uiHelper.showToastMessage("User verified. Restoring...");
                    break;
                }
            }
            else {
                if (responseMessage.equals("verify.fail")) {
                    uiHelper.showToastMessage("User not verified.");
                    break;
                }
                if (responseMessage.equals("user.exists.false")) {
                    uiHelper.showToastMessage("User does not exist.");
                    break;
                }
            }
        }
    }

    private void login(String username, String seedHashed) {
        new Thread(){
            @Override
            public void run()
            {
                String requestUrl = loginURL;
                URL url = null;
                try {
                    url = new URL(requestUrl);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("handle", username);
                params.put("seed", seedHashed);

                StringBuilder postData = new StringBuilder();

                for (Map.Entry<String,Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    try {
                        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                        postData.append('=');
                        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                // Convert string to byte array, as it should be sent
                byte[] postDataBytes = null;
                postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

                HttpURLConnection connection = null;

                try {
                    assert url != null;
                    connection = (HttpURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(false);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestProperty("Content-Length", "" + Integer.toString(postDataBytes.length));

                    //This is the user-agent of Tor Browser 8.0 on Windows, I tried to match this connection request to Tor as much as possible for maximum anonymity.
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0");

                    connection.setUseCaches (false);

                    connection.setConnectTimeout(60000);
                    connection.setReadTimeout(60000);

                    connection.connect();

                    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                    wr.write(postDataBytes);
                    wr.flush();
                    wr.close();

                    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    StringBuilder res = new StringBuilder();
                    while ((line = rd.readLine()) != null) {
                        res.append(line);
                    }

                    wr.flush();
                    wr.close();


                    responseMessage = res.toString();


                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    assert connection != null;
                    connection.disconnect();
                }
            }
        }.start();
    }

    public void registerUser() {
        responseMessage = "null";
        if(!TextUtils.isEmpty(uiHelper.handle.getText())) {
            byte[] entropy = getEntropy();
            List<String> mnemonic = null;
            try {
                mnemonic = MnemonicCode.INSTANCE.toMnemonic(entropy);
            } catch (MnemonicException.MnemonicLengthException e) {
                e.printStackTrace();
            }

            List<String> mnemonicCode = mnemonic;
            StringBuilder recoverySeed = new StringBuilder();

            assert mnemonicCode != null;
            for (int x = 0; x < mnemonicCode.size(); x++) {
                recoverySeed.append(mnemonicCode.get(x)).append(x == mnemonicCode.size() - 1 ? "" : " ");
            }

            String seedStr = recoverySeed.toString();
            String handle = uiHelper.handle.getText().toString();
            String seedHashed = HashHelper.SHA256(HashHelper.SHA256(seedStr + handle));

            checkForUser(handle);

            while (true) {
                System.out.println("Registering...");
                if(!responseMessage.equals("null")) {
                    if (responseMessage.equals("user.free")) {
                        long creationTime = 1551084297L;
                        DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(seedStr), null, "", creationTime);

                        int length = Splitter.on(' ').splitToList(seedStr).size();

                        if (length == 12) {
                            MainActivity.INSTANCE.prefs.edit().putBoolean("isNewUser", false).apply();
                            responseMessage = "null";
                            MainActivity.INSTANCE.handleStr = handle;
                            MainActivity.INSTANCE.prefs.edit().putString("handle", handle).apply();
                            MainActivity.INSTANCE.wallet.setupWalletKit(seed, handle, seedHashed);
                            uiHelper.displayDownloadContent(true);
                            uiHelper.new_wallet.setVisibility(View.GONE);
                            uiHelper.showToastMessage("Registering user...");
                        }
                    } else {
                        if (responseMessage.equals("user.exists")) {
                            uiHelper.showToastMessage("Username taken.");
                        }

                        if (responseMessage.equals("name.long")) {
                            uiHelper.showToastMessage("Username too long.");
                        }

                        if (responseMessage.equals("invalid.characters")) {
                            uiHelper.showToastMessage("Username has invalid characters.");
                        }
                    }
                    break;
                }
            }
        }
    }

    public void registerUser(String handle, String seedHashed, String serializedXpub) {
        new Thread(){
            @Override
            public void run()
            {
                String requestUrl = registerURL;
                URL url = null;
                try {
                    url = new URL(requestUrl);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                String contentType = null;

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("handle", handle); // All parameters, also easy
                params.put("seed", seedHashed); // All parameters, also easy
                params.put("xpub", serializedXpub); // All parameters, also easy

                StringBuilder postData = new StringBuilder();
                // POST as urlencoded is basically key-value pairs, as with GET
                // This creates key=value&key=value&... pairs
                for (Map.Entry<String,Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    try {
                        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                        postData.append('=');
                        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                // Convert string to byte array, as it should be sent
                byte[] postDataBytes = null;
                postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

                HttpURLConnection connection = null;

                try {
                    assert url != null;
                    connection = (HttpURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(false);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestProperty("Content-Length", "" + Integer.toString(postDataBytes.length));

                    //This is the user-agent of Tor Browser 8.0 on Windows, I tried to match this connection request to Tor as much as possible for maximum anonymity.
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0");

                    connection.setUseCaches (false);

                    connection.setConnectTimeout(60000);
                    connection.setReadTimeout(60000);

                    connection.connect();

                    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                    wr.write(postDataBytes);
                    wr.flush();
                    wr.close();

                    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    StringBuilder res = new StringBuilder();
                    while ((line = rd.readLine()) != null) {
                        res.append(line);
                    }

                    wr.flush();
                    wr.close();


                    responseMessage = res.toString();


                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    assert connection != null;
                    connection.disconnect();
                }
            }
        }.start();
    }

    public void getAddressFromHandle(String recipientAddress) {
        new Thread(){
            @Override
            public void run()
            {
                String requestUrl = getAddrURL;
                URL url = null;
                try {
                    url = new URL(requestUrl);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                String contentType = null;

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("handle", recipientAddress); // All parameters, also easy

                StringBuilder postData = new StringBuilder();
                // POST as urlencoded is basically key-value pairs, as with GET
                // This creates key=value&key=value&... pairs
                for (Map.Entry<String,Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    try {
                        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                        postData.append('=');
                        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                // Convert string to byte array, as it should be sent
                byte[] postDataBytes = null;
                postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

                HttpURLConnection connection = null;

                try {
                    assert url != null;
                    connection = (HttpURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(false);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestProperty("Content-Length", "" + Integer.toString(postDataBytes.length));

                    //This is the user-agent of Tor Browser 8.0 on Windows, I tried to match this connection request to Tor as much as possible for maximum anonymity.
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0");

                    connection.setUseCaches (false);

                    connection.setConnectTimeout(60000);
                    connection.setReadTimeout(60000);

                    connection.connect();

                    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                    wr.write(postDataBytes);
                    wr.flush();
                    wr.close();

                    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    StringBuilder res = new StringBuilder();
                    while ((line = rd.readLine()) != null) {
                        res.append(line);
                    }

                    wr.flush();
                    wr.close();


                    responseMessage = res.toString();
                    responseCode = connection.getResponseCode();


                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    assert connection != null;
                    connection.disconnect();
                }
            }
        }.start();
    }

    private void checkForUser(String handle) {
        responseMessage = "null";
        new Thread(){
            @Override
            public void run()
            {
                String requestUrl = checkForUserURL;
                URL url = null;
                try {
                    url = new URL(requestUrl);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }

                Map<String,Object> params = new LinkedHashMap<>();
                params.put("handle", handle); // All parameters, also easy

                StringBuilder postData = new StringBuilder();
                // POST as urlencoded is basically key-value pairs, as with GET
                // This creates key=value&key=value&... pairs
                for (Map.Entry<String,Object> param : params.entrySet()) {
                    if (postData.length() != 0) postData.append('&');
                    try {
                        postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                        postData.append('=');
                        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                // Convert string to byte array, as it should be sent
                byte[] postDataBytes = null;
                postDataBytes = postData.toString().getBytes(StandardCharsets.UTF_8);

                HttpURLConnection connection = null;

                try {
                    assert url != null;
                    connection = (HttpURLConnection)url.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(false);
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestProperty("Content-Length", "" + Integer.toString(postDataBytes.length));

                    //This is the user-agent of Tor Browser 8.0 on Windows, I tried to match this connection request to Tor as much as possible for maximum anonymity.
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0");

                    connection.setUseCaches (false);

                    connection.setConnectTimeout(60000);
                    connection.setReadTimeout(60000);

                    connection.connect();

                    DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                    wr.write(postDataBytes);
                    wr.flush();
                    wr.close();

                    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    StringBuilder res = new StringBuilder();
                    while ((line = rd.readLine()) != null) {
                        res.append(line);
                    }

                    wr.flush();
                    wr.close();


                    responseMessage = res.toString();


                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    assert connection != null;
                    connection.disconnect();
                }
            }
        }.start();
    }

    private static byte[] getEntropy() {
        return getEntropy(new SecureRandom());
    }

    private static byte[] getEntropy(SecureRandom random) {
        byte[] seed = new byte[DeterministicSeed.DEFAULT_SEED_ENTROPY_BITS / 8];
        random.nextBytes(seed);
        return seed;
    }


    public String getResponseMessage()
    {
        return responseMessage;
    }

    public int getResponseCode()
    {
        return responseCode;
    }

    public void setResponseMessage(String message)
    {
        this.responseMessage = message;
    }
}