package app.crescentpay.src.qr;

import android.support.v7.app.AppCompatActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.integration.android.IntentIntegrator;
import com.journeyapps.barcodescanner.CaptureActivity;

public class QRHelper
{
    public QRHelper()
    {

    }

    public void startQRScan(AppCompatActivity activity) {
        new IntentIntegrator(activity).setPrompt("Scan QR").setBeepEnabled(false).setDesiredBarcodeFormats(BarcodeFormat.QR_CODE.name()).setOrientationLocked(true).setCameraId(0).setCaptureActivity(CaptureActivity.class).initiateScan();
    }
}
