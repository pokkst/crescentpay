package app.crescentpay.src.wallet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.text.TextUtils;
import android.widget.Toast;
import android.os.Handler;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.script.Script;
import org.bitcoinj.utils.BriefLogFormatter;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.spongycastle.util.encoders.Hex;
import org.w3c.dom.Text;

import java.io.File;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.Date;

import app.crescentpay.src.MainActivity;
import app.crescentpay.src.net.NetHelper;
import app.crescentpay.src.tx.BroadcastHelper;
import app.crescentpay.src.ui.UIHelper;
import app.crescentpay.src.utils.Constants;

public class WalletHelper {
    private File walletDir;

    public NetworkParameters parameters;
    private WalletAppKit walletAppKit;
    public String serializedXpub;
    private UIHelper uiHelper;
    private NetHelper netHelper;

    public WalletHelper()
    {
        this.walletDir = new File(MainActivity.INSTANCE.getApplicationInfo().dataDir);
        parameters = Constants.IS_PRODUCTION ? MainNetParams.get() : TestNet3Params.get();
        BriefLogFormatter.init();

        uiHelper = MainActivity.INSTANCE.uiHelper;
        netHelper = MainActivity.INSTANCE.netHelper;
    }

    public void setupWalletKit(DeterministicSeed seed, String handle, String seedHashed)
    {
        setBitcoinSDKThread();
        walletAppKit = new WalletAppKit(parameters, walletDir, Constants.WALLET_NAME) {
            @Override
            protected void onSetupCompleted() {
                wallet().allowSpendingUnconfirmedTransactions();
                setupWalletListeners(wallet());

                if(MainActivity.isNewUser)
                {
                    String xpub = wallet().getWatchingKey().serializePubB58(parameters);

                    netHelper.registerUser(handle, seedHashed, xpub);

                    while(true)
                    {
                        System.out.println("Registering(2)...");

                        if(!netHelper.getResponseMessage().equals("null")) {
                            if (netHelper.getResponseMessage().equals("register.success")) {
                                MainActivity.INSTANCE.runOnUiThread(() -> Toast.makeText(MainActivity.INSTANCE, "User registered!", Toast.LENGTH_SHORT).show());
                                break;
                            } else {
                                MainActivity.INSTANCE.runOnUiThread(() -> Toast.makeText(MainActivity.INSTANCE, "An error occurred.", Toast.LENGTH_SHORT).show());
                                break;
                            }
                        }
                    }
                }
            }
        };

        walletAppKit.setDownloadListener(new DownloadProgressTracker() {
            @Override
            protected void progress(double pct, int blocksSoFar, Date date) {
                super.progress(pct, blocksSoFar, date);
                int percentage = (int) pct;
                MainActivity.INSTANCE.runOnUiThread(() -> {
                    uiHelper.displayPercentage(percentage);
                    uiHelper.displayProgress(percentage);
                });

            }

            @Override
            protected void doneDownload() {
                super.doneDownload();
                MainActivity.INSTANCE.runOnUiThread(() -> {
                    uiHelper.showToastMessage("Synced!");
                    uiHelper.displayDownloadContent(false);
                    uiHelper.refresh();
                });

                Vibrator v = (Vibrator) MainActivity.INSTANCE.getSystemService(Context.VIBRATOR_SERVICE);

                if (v != null)
                    v.vibrate(50);
            }
        });

        if(seed != null)
            walletAppKit.restoreWalletFromSeed(seed);

        walletAppKit.setBlockingStartup(false);
        walletAppKit.startAsync();
    }

    public Wallet getWallet()
    {
        return walletAppKit.wallet();
    }

    public String getXPUB()
    {
        return serializedXpub;
    }

    private void setBitcoinSDKThread() {
        final Handler handler = new Handler();
        Threading.USER_THREAD = handler::post;
    }

    public Coin getBalance(Wallet wallet)
    {
        return wallet.getBalance(Wallet.BalanceType.ESTIMATED);
    }

    private boolean isValidAddress(String addr) {
        try {
            Address.fromBase58(parameters, addr);
            return true;
        } catch (AddressFormatException e) {
            return false;
        }
    }

    public void send() {
        netHelper.setResponseMessage("null");
        if(!uiHelper.isDisplayingDownload()) {
            String recipientAddress = uiHelper.getRecipient();
            String amount = uiHelper.getAmount();
            double amtDblToFrmt;

            if (!TextUtils.isEmpty(amount)) {
                amtDblToFrmt = Double.parseDouble(amount);
            } else {
                amtDblToFrmt = 0;
            }

            DecimalFormat df = new DecimalFormat("#.########");
            df.setRoundingMode(RoundingMode.CEILING);

            String amtDblFrmt = df.format(amtDblToFrmt);
            double amtDbl = Double.parseDouble(amtDblFrmt);

            int feeInt;

            if(!TextUtils.isEmpty(uiHelper.feeText.getText().toString()))
                feeInt = Integer.parseInt(uiHelper.feeText.getText().toString());
            else
                feeInt = 0;

            if (TextUtils.isEmpty(recipientAddress)) {
                uiHelper.showToastMessage("Please enter a recipient.");
            } else if (TextUtils.isEmpty(amount) || amtDbl <= 0.000001) {
                uiHelper.showToastMessage("Enter a valid amount. Minimum is 0.000001 BTC");
            } else if (TextUtils.isEmpty(uiHelper.feeText.getText().toString())) {
                uiHelper.showToastMessage("Please set fee!");
                uiHelper.clearAmount();
            } else if(feeInt != 0 && feeInt <= 9) {
                uiHelper.showToastMessage("Fee minimum is 10 sat/byte!");
                uiHelper.feeText.setText(null);
            } else if (getBalance(walletAppKit.wallet()).isLessThan(Coin.parseCoin(amtDblFrmt))) {
                uiHelper.showToastMessage("You do not have enough Bitcoin!");
                uiHelper.clearAmount();
            } else {
                if (!isValidAddress(recipientAddress)) {
                    if(!recipientAddress.startsWith("$"))
                    {
                        uiHelper.showToastMessage("Handle must include $!");
                        uiHelper.displayRecipientAddress(null);
                    }
                    else
                    {
                        recipientAddress = recipientAddress.replace("$", "");

                        ConnectivityManager connectivityManager
                                = (ConnectivityManager) MainActivity.INSTANCE.getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

                        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                            netHelper.getAddressFromHandle(recipientAddress);

                            while(true) {

                                if(activeNetworkInfo.isConnected()) {
                                    //This line is crucial. If you remove it, the while loop gets stuck. I'm too lazy to look at why this happens.
                                    System.out.println("Grabbing address...");

                                    if (netHelper.getResponseCode() != -1 && netHelper.getResponseCode() == 200) {
                                        if (!netHelper.getResponseMessage().equals("null")) {
                                            if (netHelper.getResponseMessage().equals("user.exists.false")) {
                                                uiHelper.showToastMessage("User does not exist!");
                                            } else {
                                                String userAddress = netHelper.getResponseMessage();
                                                System.out.println(userAddress);
                                                Coin coinAmt = Coin.parseCoin(amtDblFrmt);

                                                if (coinAmt.getValue() > 0.0) {
                                                    Address destination = Address.fromBase58(parameters, userAddress);
                                                    SendRequest req;

                                                    if (coinAmt.equals(getBalance(walletAppKit.wallet()))) {
                                                        req = SendRequest.emptyWallet(destination);
                                                    } else {
                                                        req = SendRequest.to(destination, Coin.parseCoin(amtDblFrmt));
                                                    }

                                                    req.ensureMinRequiredFee = false;
                                                    //converting because the UI uses sats/byte, so we need to convert that to fee per kb here
                                                    req.feePerKb = Coin.valueOf(Long.parseLong(feeInt + "") * 1000L);
                                                    try {
                                                        Transaction tx = getWallet().sendCoinsOffline(req);
                                                        byte[] txHexBytes = Hex.encode(tx.bitcoinSerialize());
                                                        String txHex = new String(txHexBytes, StandardCharsets.UTF_8);
                                                        System.out.println("Created raw transaction: " + txHex);

                                                        /*
                                                          I use the Samourai Wallet pushTx API because I've had issues sometimes where transactions sent through
                                                          BitcoinJ itself to the wallet peers would not broadcast effectively, or at all. This sends a raw transaction hex to the
                                                          Samourai API and then Samourai Wallet broadcasts the transaction to the network.
                                                         */
                                                        System.out.println("Broadcasting raw transaction...");
                                                        BroadcastHelper helper = new BroadcastHelper(Constants.IS_PRODUCTION);
                                                        helper.broadcast(txHex);

                                                    } catch (InsufficientMoneyException e) {
                                                        e.printStackTrace();
                                                        uiHelper.showToastMessage(e.getMessage());
                                                    }
                                                }
                                            }

                                            break;
                                        }
                                    } else if (netHelper.getResponseCode() != -1) {
                                        uiHelper.showToastMessage("An error occurred. Please try again.");
                                        break;
                                    }
                                }
                                else
                                {
                                    uiHelper.showToastMessage("No Internet connection!");
                                    break;
                                }
                            }
                        }
                        else
                        {
                            uiHelper.showToastMessage("No Internet connection!");
                        }
                    }
                } else {
                    Coin coinAmt = Coin.parseCoin(amtDblFrmt);

                    if (coinAmt.getValue() > 0.0) {
                        Address destination = Address.fromBase58(parameters, recipientAddress);
                        SendRequest req;

                        if (coinAmt.equals(getBalance(walletAppKit.wallet()))) {
                            req = SendRequest.emptyWallet(destination);
                        } else {
                            req = SendRequest.to(destination, Coin.parseCoin(amtDblFrmt));
                        }

                        req.ensureMinRequiredFee = false;
                        //converting because the UI uses sats/byte, so we need to convert that to fee per kb here
                        req.feePerKb = Coin.valueOf(Long.parseLong(feeInt + "") * 1000L);
                        try {
                            Transaction tx = getWallet().sendCoinsOffline(req);
                            byte[] txHexBytes = Hex.encode(tx.bitcoinSerialize());
                            String txHex = new String(txHexBytes, StandardCharsets.UTF_8);
                            System.out.println("Created raw transaction: " + txHex);

                            /*
                              I use the Samourai Wallet pushTx API because I've had issues sometimes where transactions sent through
                              BitcoinJ itself to the wallet peers would not broadcast effectively, or at all. This sends a raw transaction hex to the
                              Samourai API and then Samourai Wallet broadcasts the transaction to the network.
                             */
                            System.out.println("Broadcasting raw transaction...");
                            BroadcastHelper helper = new BroadcastHelper(Constants.IS_PRODUCTION);
                            helper.broadcast(txHex);

                        } catch (InsufficientMoneyException e) {
                            e.printStackTrace();
                            uiHelper.showToastMessage(e.getMessage());
                        } catch (Wallet.CouldNotAdjustDownwards e) {
                            e.printStackTrace();
                            uiHelper.showToastMessage("Not enough Bitcoin for fee!");
                        } catch (Wallet.ExceededMaxTransactionSize e) {
                            e.printStackTrace();
                            uiHelper.showToastMessage("Transaction is too large!");
                        }
                    }
                }
            }
        }
    }

    private void setupWalletListeners(Wallet wallet) {
        wallet.addCoinsReceivedEventListener((wallet1, tx, prevBalance, newBalance) -> {
            if(!uiHelper.isDisplayingDownload()) {
                uiHelper.displayMyBalance(getBalance(wallet1).toFriendlyString());

                if (tx.getPurpose() == Transaction.Purpose.UNKNOWN)
                    uiHelper.showToastMessage("Received " + newBalance.minus(prevBalance).toFriendlyString());

                Vibrator v = (Vibrator) MainActivity.INSTANCE.getSystemService(Context.VIBRATOR_SERVICE);

                if (v != null)
                    v.vibrate(100);

                uiHelper.refresh();
            }
        });
        wallet.addCoinsSentEventListener((wallet12, tx, prevBalance, newBalance) -> {
            if(!uiHelper.isDisplayingDownload()) {
                uiHelper.displayMyBalance(getBalance(wallet12).toFriendlyString());
                uiHelper.clearAmount();
                uiHelper.displayRecipientAddress(null);
                uiHelper.showToastMessage("Sent " + prevBalance.minus(newBalance).minus(tx.getFee()).toFriendlyString());
                uiHelper.refresh();
            }
        });
    }
}
