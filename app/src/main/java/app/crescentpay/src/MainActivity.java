package app.crescentpay.src;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.protocols.payments.PaymentProtocolException;
import org.bitcoinj.protocols.payments.PaymentSession;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import app.crescentpay.src.net.NetHelper;
import app.crescentpay.src.ui.UIHelper;
import app.crescentpay.src.utils.PermissionHelper;
import app.crescentpay.src.wallet.WalletHelper;

public class MainActivity extends AppCompatActivity {
    public static MainActivity INSTANCE;
    public UIHelper uiHelper;
    public WalletHelper wallet;
    public NetHelper netHelper;

    public ArrayList<Transaction> txList;
    public SharedPreferences prefs = null;
    public static boolean isNewUser = true;
    public String handleStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        INSTANCE = this;
        this.setContentView(R.layout.activity_main);
        this.uiHelper = new UIHelper();
        this.netHelper = new NetHelper();
        this.wallet = new WalletHelper();

        this.prefs = getSharedPreferences("app.crescentpay.src", MODE_PRIVATE);

        isNewUser = this.prefs.getBoolean("isNewUser", true);

        if(isNewUser) {
            this.uiHelper.newuser.setVisibility(View.VISIBLE);
        } else {
            this.handleStr = prefs.getString("handle", handleStr);
            this.wallet.setupWalletKit(null, "", "");
            this.uiHelper.displayDownloadContent(true);
        }

        PermissionHelper permissionHelper = new PermissionHelper();
        permissionHelper.askForPermissions(this, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String address = scanResult.getContents();
            if(address != null) {
                /*
                  In Bitcoin Core at least, when generating a request payment, the QR code has the prefix "bitcoin:" and the address added onto it.
                  We remove that here to get the raw address.
                 */
                if (address.contains("bitcoin:")) {
                    String addressFixed = address.replace("bitcoin:", "");

                    if (address.contains("?r=")) {
                        String addressFixed2 = addressFixed.replace("?r=", "");
                        this.uiHelper.displayRecipientAddress(addressFixed2);
                        String url = addressFixed2;
                        ListenableFuture<PaymentSession> future;

                        if (url.startsWith("https")) {
                            try {
                                future = PaymentSession.createFromUrl(url);

                                PaymentSession session = future.get();

                                Coin amountWanted = session.getValue();
                                this.runOnUiThread(() -> {
                                    uiHelper.getAmountText().setText(amountWanted.toPlainString());
                                });
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            } catch (PaymentProtocolException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else
                    {
                        this.uiHelper.displayRecipientAddress(address);
                    }
                } else {
                    uiHelper.displayRecipientAddress(address);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {

        if(this.uiHelper.new_wallet.getVisibility() == View.VISIBLE) {
            this.uiHelper.new_wallet.setVisibility(View.GONE);
            this.uiHelper.newuser.setVisibility(View.VISIBLE);
        }
        else if(this.uiHelper.restore_wallet.getVisibility() == View.VISIBLE) {
            this.uiHelper.restore_wallet.setVisibility(View.GONE);
            this.uiHelper.newuser.setVisibility(View.VISIBLE);
        }
        else if(this.uiHelper.sendWindow.getVisibility() == View.VISIBLE) {
            this.uiHelper.sendWindow.setVisibility(View.GONE);
            this.uiHelper.clearAmount();
            this.uiHelper.displayRecipientAddress(null);
        }
        else if(this.uiHelper.receiveWindow.getVisibility() == View.VISIBLE) {
            this.uiHelper.receiveWindow.setVisibility(View.GONE);
        }
        else if(this.uiHelper.historyWindow.getVisibility() == View.VISIBLE) {
            this.uiHelper.historyWindow.setVisibility(View.GONE);
        }
        else if(this.uiHelper.walletSettings.getVisibility() == View.VISIBLE) {
            this.uiHelper.walletSettings.setVisibility(View.GONE);
        }
        else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
